#!/usr/bin/env python3

""" Skript faasikoste määramisel kasutavate parameetrite optimeerimiseks. """

import os
import src.slmcalib2 as sc2
import numpy as np


# Teha esimene mõõtmine

os.system(r".\run.py ./2018-07-07/first/")

# Selle mõõtmise põhjal arvutada uued difraktsioonivõred erinevate kaliibrimismeetoditega

LOC = {"start_x":314, "start_y":160, "end_x":1628, "end_y":1901}
ANALYSER = sc2.DataAnalysis("./2018-07-07/first/", **LOC)

print("\nAlustan 16_1")
CALIBRATOR = sc2.Calibration(ANALYSER, 16, stdev=1)
np.save("./16_1.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/16_1/ -series 16_1.npy")

print("\nAlustan 16_25")
CALIBRATOR = sc2.Calibration(ANALYSER, 16, stdev=2.5)
np.save("./16_25.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/16_25/ -series 16_25.npy")

print("\nAlustan 16_4")
CALIBRATOR = sc2.Calibration(ANALYSER, 16, stdev=4)
np.save("./16_4.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/16_4/ -series 16_4.npy")

print("\nAlustan 32_1")
CALIBRATOR = sc2.Calibration(ANALYSER, 32, stdev=1)
np.save("./32_1.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/32_1/ -series 32_1.npy")

print("\nAlustan 32_25")
CALIBRATOR = sc2.Calibration(ANALYSER, 32, stdev=2.5)
np.save("./32_25.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/32_25/ -series 32_25.npy")

print("\nAlustan 32_4")
CALIBRATOR = sc2.Calibration(ANALYSER, 32, stdev=4)
np.save("./32_4.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/32_4/ -series 32_4.npy")

print("\nAlustan 64_1")
CALIBRATOR = sc2.Calibration(ANALYSER, 64, stdev=1)
np.save("./64_1.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/64_1/ -series 64_1.npy")

print("\nAlustan 64_25")
CALIBRATOR = sc2.Calibration(ANALYSER, 64, stdev=2.5)
np.save("./64_25.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/64_25/ -series 64_25.npy")

print("\nAlustan 64_4")
CALIBRATOR = sc2.Calibration(ANALYSER, 64, stdev=4)
np.save("./64_4.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./2018-07-07/64_4/ -series 64_4.npy")

print("Valmis!")
