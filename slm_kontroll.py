#!/usr/bin/env python3

""" Skript faasikoste määramisel kasutavate parameetrite optimeerimiseks. """

import os
import src.slmcalib2 as sc2
import numpy as np


# Teha esimene mõõtmine

DATE = "2018-07-12"

os.system(r".\run.py ./"+DATE+"/check1/")

# Selle mõõtmise põhjal arvutada uued difraktsioonivõred erinevate kaliibrimismeetoditega

LOC = {"start_x":314, "start_y":160, "end_x":1628, "end_y":1901}
ANALYSER_1 = sc2.DataAnalysis("./"+DATE+"/check1/", **LOC)

CALIBRATOR = sc2.Calibration(ANALYSER_1, 32, stdev=2.5)
np.save("./check1.npy", CALIBRATOR.generate_phasecalib_array(CALIBRATOR.calib_boxing))
os.system(r".\run.py ./"+DATE+"/check2/ -series check1.npy")

ANALYSER_2 = sc2.DataAnalysis("./"+DATE+"/check2/", **LOC)

import matplotlib.pyplot as plt

for phaserow in ANALYSER_2.get_phase_boxes(32):
    for phase in phaserow:
        plt.plot(sc2.PHASE_RANGE, phase[2:])

plt.plot(sc2.PHASE_RANGE, sc2.PHASE_RANGE, lw=2, color="purple")
plt.show()