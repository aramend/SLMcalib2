"""Moodul, mis sisaldab klasse SLMi faasikoste määramisega seotud andmetöötluseks."""

import os
import sys
from importlib import import_module
from scipy.ndimage.filters import gaussian_filter1d
from scipy.interpolate import RectBivariateSpline
import cv2
import numpy as np
import sympy as sp

ZERO_GREYSCALE  = 20                                         # Nullfaasile vastav halltoon.
PVAL_RANGE      = np.arange(ZERO_GREYSCALE, 256)             # Vaadeldav halltoonide vahemik.
PVAL_RANGE_SIZE = len(PVAL_RANGE)                            # Halltoonide vahemiku suurus.
PHASE_RANGE     = np.linspace(0, 2.3*np.pi, PVAL_RANGE_SIZE) # Vastav faaside vahemik, mida kasutada teisel kaliibrimisel.
POLY_DEGREE     = 7                                          # Kaliibrimisel kasutatava polünoomi järk.
BARLEN          = 4                                          # Difraktsioonivõre perioodi laius.

def progress(ratio):
    """Funktsioon mõõtmiste ajal programmi töö jälgimiseks.

    Argumendid:
    ratio -- float; Tehtud töö suhe tegemata tööga.
    """

    progress_bar_length = 30
    percentage = str(np.round(ratio * 100, 2)) + "%"
    filled = int(np.ceil((ratio) * progress_bar_length))

    sys.stdout.write("\r[%s%s] %s " % ("#" * filled, "-" * (progress_bar_length - filled),
                                       percentage))
    sys.stdout.flush()

def get_rms_error(phase_boxes):
    """Funktsioon, mis leiab iga faasikoste jaoks ruutkeskmise hälbe ideaalsest y=x kostest.
    Seejärel leitakse ruutkeskmiste vigade ruutkeskmine hälbe. Tagastatakse vaid 1 arv, mis
    kirjeldab selle meetodi viga.

    Argumendid:
    phase_boxes -- numpy.ndarray; Sisendiks olevad faasikosted, mille viga hindame.
    """

    rms_error = lambda error_data: np.sqrt(np.sum(error_data**2)/PVAL_RANGE_SIZE)

    n_box1d = len(phase_boxes)
    errors = []

    for i in range(n_box1d):
        for j in range(n_box1d):

            phase_curve = phase_boxes[i, j, 2:]
            error_list = np.absolute(phase_curve - PHASE_RANGE)

            errors.append(rms_error(np.array(error_list)))

    return rms_error(np.array(errors))

class DataAnalysis:
    """SLM  faasikoste  määramiseks  mõõdetud  andmete  analüüsi  klass.

    Klassi  isendi  tekitamisel loetakse sisse pilte
    memmap andmemassiiviks, mille peal andmetöötlust
    klassi isendifunktsioonidega läbi viia.
    """
    # pylint: disable=too-many-instance-attributes

    def __init__(self, foldername, start_x=0, start_y=0, end_x=0, end_y=0, memmap=0):
        """Klassi initsialiseerimise funktsioon.

        Argumendid:
        foldername -- str; Pilte sisaldava kausta nimi, eg. "./2018-06-25/".
        start_x    -- int; Huvi pakkuva piirkonna ülemise vasaku nurga x koordinaat.
        start_y    -- int; Huvi pakkuva piirkonna ülemise vasaku nurga y koordinaat.
        end_x      -- int; Huvi pakkuva piirkonna alumise parema nurga x koordinaat.
        end_y      -- int; Huvi pakkuva piirkonna alumise parema nurga y koordinaat.
        """

        self.foldername = foldername

        if 0 not in (start_x, start_y, end_x, end_y):
            self.start_x, self.start_y, self.end_x, self.end_y = start_x, start_y, end_x, end_y
        else:
            self.start_x, self.start_y, self.end_x, self.end_y = self.determine_loc()
            print("Valitud piirkond:", self.start_x, self.start_y, self.end_x, self.end_y)

        self.len_x = self.end_x - self.start_x
        self.len_y = self.end_y - self.start_y

        if memmap == 0:
            filtered = list(filter(lambda a: a != "", foldername.split("/")))
            self.refname = "datarr_"+filtered[-1]
            self.read_in()
            self.dataref = np.memmap("../"+self.refname, dtype=np.uint16, mode="r",
                                     shape=(PVAL_RANGE_SIZE, self.len_x, self.len_y))
        else:
            self.dataref = np.memmap("../"+memmap, dtype=np.uint16, mode="r",
                                     shape=(PVAL_RANGE_SIZE, self.len_x, self.len_y))

    def determine_loc(self):
        """Määrame cv2 ROI funktsiooniga SLM'i asukoha kaamera tehtud piltidel."""

        edgeimg = cv2.imread(self.foldername+"/edge_img.tiff", -1)

        cv2.namedWindow("ROI", cv2.WINDOW_NORMAL)
        y_loc, x_loc, width, height = cv2.selectROI("ROI", edgeimg, False)
        cv2.destroyWindow("ROI")

        if 0 in (y_loc, x_loc, width, height):
            raise Exception("Kast valesti valitud, proovi uuesti.")

        xratio, yratio = height/(768-40), width/(1024-40)

        truebwx, truebwy = int(40*xratio), int(40*yratio)

        return x_loc-truebwx, y_loc-truebwy, x_loc+height+truebwx, y_loc+width+truebwy

    def read_in(self):
        """Loeme pildid sisse kõvakettale 3D numpy massiiviks, millele viitame memmapiga."""

        files = os.listdir(self.foldername)

        datamap = np.memmap("../"+self.refname, dtype=np.uint16, mode="w+",
                            shape=(PVAL_RANGE_SIZE, self.len_x, self.len_y))
        minval = 1e+9

        for n_ind in range(PVAL_RANGE_SIZE):
            idx = np.where(np.core.defchararray.startswith(files, "img_"+str(n_ind+1)+"_"))[0][0]
            chosenfile = files[idx]
            img = cv2.imread(self.foldername+"/"+chosenfile, -1)
            if np.min(img) < minval:
                minval = np.min(img)
            datamap[n_ind, :, :] = img[self.start_x : self.end_x, self.start_y : self.end_y]
        datamap -= int(minval)

        del datamap

    @staticmethod
    def smoothing(input_data, stdev):
        """Funktsioon mõõteandmete silumiseks gaussi kerneliga, et andmetöötlus oleks viisakam.

        Funktsioon   kasutab  otse  scipy  funktsiooni,   kuid   siin
        viiakse   läbi  ka   silutud  kõvera   kaheti   normeerimine.

        Argumendid:
        input_data -- Mürased andmed, mis vajavad silumist.
        stdev      -- float; Scipy funktsioonile edasiantav Gaussi kerneli standardhälve.
        """

        def fix(chunk):
            """Andmetest lahutatakse miinimumväärtus ja normeeritakse.

            Argumendid:
            chunk -- Sisendlõik andmetest.
            """
            chunk = chunk - np.min(chunk)
            return chunk / np.max(chunk)

        filtered = gaussian_filter1d(input_data, stdev)

        maxidx = np.where(filtered == np.max(filtered))[0][0]
        filtered1 = fix(filtered[:maxidx+1])
        filtered2 = fix(filtered[maxidx:])[1:]

        return np.append(filtered1, filtered2)

    def data_box(self, row_start, row_end, col_start, col_end, mode="norm"):
        """Üldine funktsioon mingist alapiirkonnast kas keskmistatud,
        normeeritud või kaheti normeeritud andmete saamiseks.

        Argumendid:
        row_start -- int; Reaindeksi algus.
        row_end   -- int; Reaindeksi lõpp.
        col_start -- int; Veeruindeksi algus.
        col_end   -- int; Veeruindeksi lõpp.
        mode      -- str; Režiim, milles andmeid leitakse
        """

        pixvals = [np.mean(self.dataref[_, row_start : row_end, col_start : col_end])\
                           for _ in range(len(self.dataref))]

        if mode == "mean":
            return np.array(pixvals)

        pixvals -= min(pixvals)

        if mode == "norm":
            maxval = max(pixvals)
            return np.array([np.abs(val/maxval) for val in pixvals])

        if mode == "double_norm":
            max_mean_ind = np.where(pixvals == np.max(pixvals))[0][0]

            mean_vals1 = pixvals[:max_mean_ind+1]
            mean_vals1 = mean_vals1 - np.min(mean_vals1)

            mean_vals2 = pixvals[max_mean_ind+1:]
            mean_vals2 = mean_vals2 - np.min(mean_vals2)

            norm_vals1 = [val/max(mean_vals1) for val in mean_vals1]
            norm_vals2 = [val/max(mean_vals2) for val in mean_vals2]

            return np.array(np.append(norm_vals1, norm_vals2))

    def get_boxes(self, n_box1d, mode="norm"):
        """ Etteantud režiimis n*n kastide saamine.

        Argumendid:
        n_box1d -- int; Soovitud kastide arv piki ühte telge. Tagastatakse n_box1d^2 kasti.
        mode    -- str; Režiim, milles andmeid leida.
        """

        modes = ["norm", "double_norm", "mean"]
        if mode not in modes:
            raise Exception("Vigane režiim!")

        box_x = int(np.floor(self.len_x/n_box1d))
        box_y = int(np.floor(self.len_y/n_box1d))

        box_data_list = []

        for row_ind in range(n_box1d):
            for col_ind in range(n_box1d):

                row_start = row_ind * box_x
                if self.len_x - (row_ind + 1) * box_x < box_x:
                    row_end = self.len_x - 1
                else:
                    row_end = (row_ind + 1) * box_x

                col_start = col_ind * box_y
                if self.len_y - (col_ind + 1) * box_y < box_y:
                    col_end = self.len_y - 1
                else:
                    col_end = (col_ind+1) * box_y

                x_loc = (row_start + (row_end - row_start)/2) * 768/self.len_x
                y_loc = (col_start + (col_end - col_start)/2) * 1024/self.len_y

                loc = [x_loc, y_loc]

                box_data_list.append(np.append([loc], self.data_box(row_start,\
                                     row_end, col_start, col_end, mode=mode)))

        return np.array(box_data_list)

    def mean_data(self):
        """Tagastab piltide keskmistatud intensiivsused kogu mõõteseeriast."""

        mean_vals = self.get_boxes(1, mode="mean")
        return np.array(mean_vals[0, 2:])

    def norm_data(self):
        """Tagastab piltide normeeritud intensiivsused kogu mõõteseeriast."""

        norm_vals = self.get_boxes(1, mode="norm")
        return np.array(norm_vals[0, 2:])

    def double_norm_data(self):
        """Andmete normeerimine mõlemal pool maksimumi."""

        dnorm_vals = self.get_boxes(1, mode="double_norm")
        return np.array(dnorm_vals[0, 2:])

    def phase_data(self, normdat, stdev):
        """Normeeritud intensiivsuskõverat silutakse ja arvutatakse faasikoste.

        Argumendid:
        normdat -- Normeeritud intensiivsuskõver.
        """

        loc_x = normdat[0]
        loc_y = normdat[1]

        # Silume mõõteandmeid Gaussi kerneliga.
        normdat = normdat[2:]
        normdat = self.smoothing(normdat, stdev)

        norm_maxidx = np.argmax(normdat)
        norm1 = normdat[:norm_maxidx+1]
        norm2 = normdat[norm_maxidx+1:]

        # Leiame norm2 miinimumi
        norm2_minidx = np.argmin(norm2)
        norm21 = norm2[:norm2_minidx+1]
        norm22 = norm2[norm2_minidx+1:]

        phase1 = [2*np.arcsin(np.sqrt(val)) for val in norm1]
        phase2 = [2*np.pi - 2*np.arcsin(np.sqrt(val)) for val in norm21]
        phase3 = [2*np.pi + 2*np.arcsin(np.sqrt(val)) for val in norm22]

        phasedat = np.insert(phase3, 0, np.insert(phase2, 0, phase1))

        return np.append([loc_x, loc_y], phasedat)


    def get_phase_boxes(self, n_box1d, mode="double_norm", stdev=1):
        """n_box1d^2 kastide faasikostete leidmine.

        Argumendid:
        n_box1d -- int; Soovitud kastide arv piki ühte telge. Tagastatakse n_box1d^2 kastide
        faasikoste.
        """

        norm_datas = self.get_boxes(n_box1d, mode=mode)
        newshape = (n_box1d, n_box1d, PVAL_RANGE_SIZE+2)

        return np.array([self.phase_data(norm_data, stdev) for norm_data in norm_datas]).reshape(newshape)

class DataGet:
    """Kaameraga suhtlemiseks ja piltide tegemiseks klass."""

    def __init__(self, outputfolder):
        """
        Argumendid:
        outputfolder -- str; Sihtkaust, kuhu pilte salvestatakse.
        """

        self.pc2 = import_module("PyCapture2")
        self.fname = outputfolder
        bus = self.pc2.BusManager()
        uid = bus.getCameraFromIndex(0)
        self.kaamera = self.pc2.Camera()
        self.kaamera.connect(uid)

        prop_dict = {#self.pc2.PROPERTY_TYPE.BRIGHTNESS: 5.0, # Terve pildi lineaarne heledus
            self.pc2.PROPERTY_TYPE.AUTO_EXPOSURE: 0.85, # EV heleduse keskpunkti määramine
            self.pc2.PROPERTY_TYPE.GAMMA: 1.0, # Mittelineaarne heledus Gamma 6
            self.pc2.PROPERTY_TYPE.SHUTTER: 1000, # Säriaeg ms
            self.pc2.PROPERTY_TYPE.GAIN: 0.0} # ccd signaalivõimendus

        for prop in prop_dict:
            self.kaamera.setProperty(type=prop, absValue=prop_dict[prop], autoManualMode=False)

    @staticmethod
    def edge_dif_mask():
        """Pildi servasi joonistav mask, millega määrata SLMi asukoht kaamera piltidel."""

        border_width = 40

        x_grid, y_grid = np.ogrid[0:768, 0:1024]

        bxor1 = lambda x, y: np.logical_xor(x < border_width, y < border_width)
        bxor2 = lambda x, y: np.logical_xor(x > 767-border_width, y > 1023-border_width)
        difmask = lambda x, y: y//4 % 2 == np.logical_xor(bxor1(x_grid, y_grid),\
                                                          bxor2(x_grid, y_grid))

        mask = difmask(x_grid, y_grid)*130
        mask[mask == 0] = ZERO_GREYSCALE

        return mask.astype(np.uint8)

    @staticmethod
    def pval_grid_mask(val):
        """Funktsioon, mis loob difraktsioonivõre faasimaski SLMile kuvamiseks.

        Argumendid:
        val -- int; Difraktsioonivõre aktiivsete ribade halltooni väärtus.
        """

        img = np.full((768, 1024), ZERO_GREYSCALE, dtype=np.uint8)

        for i in range(1024 // BARLEN):
            if i % 2 == 0:
                img[:, i * BARLEN:(i+1) * BARLEN] = val

        return img

    def take_pic(self, targetname):
        """Funktsioon, mis salvestab kaamerast puhvri pildiks.

        Argumendid:
        targetname -- str; Asukoht, kuhu pilt salvestatakse.
        """

        self.kaamera.startCapture()

        image = self.kaamera.retrieveBuffer()

        newimg = image.convert(self.pc2.PIXEL_FORMAT.MONO16)
        newimg.save(targetname.encode("utf-8"), self.pc2.IMAGE_FILE_FORMAT.TIFF)

        self.kaamera.stopCapture()

    def single(self, input_arr, img_name):
        """Funktsioon üksiku pildi tegemiseks faasimaskist, mida kuvatakse SLMile.

        Argumendid:
        inputarr -- numpy.ndarray; SLMile kuvatava faasimaski numpy massiiv.
        filename -- str; Nimi, millega tehtav pilt salvestada.
        """

        cv2.namedWindow("Mask", cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty("Mask", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

        cv2.imshow("Mask", input_arr.astype(np.uint8))
        key = cv2.waitKey(1500)
        if key == -1:
            self.take_pic(self.fname + img_name)

        cv2.imshow("Mask", input_arr)

    def series(self, input_arr_name):
        """Funktsioon n pildi seeria mõõtmiseks n * 2D massiivist.

        Argumendid:
        input_arr_name -- str; sisendmassiivi nimi, tihti .npy lõpuga.
        """

        datarr = np.load(input_arr_name)
        size = len(datarr)

        for i, mask_img in enumerate(datarr):
            progress((i+1)/size)
            self.single(mask_img, "img_" + str(i+1) + "_.tiff")

    def measure(self):
        """Funktsioon, mis viib läbi difraktsiooni efektiivsuse mõõtmisi n_vals halltoonil.
        """

        self.single(self.edge_dif_mask(), "edge_img.tiff")

        for i, pval in enumerate(PVAL_RANGE):
            done = np.round((i+1)/PVAL_RANGE_SIZE, decimals=2)
            progress(done)
            mask_img = self.pval_grid_mask(pval)
            self.single(mask_img, "img_" + str(i+1) + "_" + str(pval) + ".tiff")

        # Klassi isendit kasutades peale mõõtmisi kasutada isend.kaamera.disconnect()

class Calibration:
    """Tehtud piltidest kaliibrimiskõverate leidmise klass. Kõveraid saab
    hiljem kasutada faasimaskide halltoonpiltideks teisendamisel.
    """

    def __init__(self, analysis_instance, n_box1d, stdev=1):
        """
        Argumendid:
        analysis_instance -- DataAnalysis; Andmete analüüsi DataAnalysis klassi isend,
                             mida kasutatakse kaliibrimiskõverate leidmisel.
        n_box1d           -- int; Soovitud kastide arv piki ühte telge. Kaliibrimiskõverad
                             leitakse n_box1d^2 kastide faasikostetest.
        """

        self.analysis_instance = analysis_instance
        self.n_box1d = n_box1d
        self.phase_boxes = self.analysis_instance.get_phase_boxes(n_box1d, stdev=stdev)

        self.poly_lambda = self.poly3d_func(POLY_DEGREE)
        self.poly_coeffs = self.get_coeffs()

        self.global_phase_curve = self.analysis_instance.get_phase_boxes(1)[0, 0, 2:]
        self.loc_2pi = PVAL_RANGE[self.closest_pval_idx(self.global_phase_curve, 2*np.pi)]

        self.phases_x = [self.phase_boxes[i, 0, 0] for i in range(n_box1d)]
        self.phases_y = [self.phase_boxes[0, j, 1] for j in range(n_box1d)]
        self.interp_funcs = self.__get_funcs()
        self.phase_to_idx = np.vectorize(lambda phase: np.argmin(np.absolute(PHASE_RANGE-phase)))
        self.transform = np.vectorize(lambda idx, x, y: self.interp_funcs[idx](x, y))

    @staticmethod
    def closest_pval_idx(phase_data, target_phases):
        """Funktsioon faasimodulatsioonile 2pi lähima vastava halltooni leidmiseks.

        Argumendid:
        phase_data    -- Halltoon-faas andmed, millest otsime lähimat faasi.
        target_phases -- numpy.ndarray, float; Sihtfaaside massiiv või üks sihtfaas.
        """

        if isinstance(target_phases, np.ndarray):

            targets_number = len(target_phases.flatten())
            tiled_phases = np.tile(phase_data, (targets_number, 1)).reshape(target_phases.shape[0],
                                                                            target_phases.shape[1],
                                                                            len(phase_data))
            tiled_target_phases = np.einsum("kij->ijk", np.tile(target_phases, (len(phase_data),
                                                                                1, 1)))
            diff = np.absolute(tiled_phases - tiled_target_phases)
            minima = np.argmin(diff, axis=2)

            return minima.flatten()

        else:

            return np.argmin(np.absolute(phase_data - target_phases))

    @staticmethod
    def poly3d_func(degree):
        """Funktsioon, mis tagastab lambda funktsiooni kiireks polünoomi arvutamiseks.

        Argumendid:
        degree -- int; Soovitud polünoomi järk.
        """

        arg_x, arg_y, arg_phi = sp.symbols(r"x y \phi")
        powerslist = []
        for i in range(degree+1):
            [powerslist.append(np.array(_)) for _ in sp.Poly((arg_x + arg_y + arg_phi)**i,
                                                             (arg_x, arg_y, arg_phi)).monoms()]

        polynomial = sum([arg_x**i * arg_y**j * arg_phi**k for i, j, k in powerslist])
        func = sp.sympify(polynomial, locals={r"x":arg_x, r"y":arg_y, r"\phi":arg_phi})

        return sp.lambdify((arg_x, arg_y, arg_phi), func.args)

    @staticmethod
    def phase_grid_mask(phase_value):
        """Funktsioon, mis loob difraktsioonivõre faasimaski SLMile kuvamiseks.

        Argumendid:
        val -- int; Difraktsioonivõre aktiivsete ribade halltooni väärtus.
        """

        img = np.full((768, 1024), 0, dtype=np.float32)

        for i in range(1024 // BARLEN):
            if i % 2 == 0:
                img[:, i * BARLEN:(i+1) * BARLEN] = phase_value

        return img

    def __get_funcs(self):
        """Funktsioon, mis tagastab parima interpoleerimisfunktsiooni iga faasi väärtuse jaoks.
        """
        # Leiame halltoonide pildi iga faasitasandi jaoks

        def closest_pval_slice(target_phase):
            """Abifunktsioon halltoonide leidmisel"""

            img = np.empty((self.n_box1d, self.n_box1d))

            for i in range(self.n_box1d):
                for j in range(self.n_box1d):
                    idx = np.argmin(np.absolute(self.phase_boxes[i, j, 2:] - target_phase))
                    img[i, j] = PVAL_RANGE[idx]

            return img

        funcs = []

        for phase_val in PHASE_RANGE:
            func = RectBivariateSpline(self.phases_x, self.phases_y,
                                       closest_pval_slice(phase_val))
            funcs.append(func)

        return np.array(funcs)

    def get_coeffs(self):
        """Funktsioon võtab sisendiks alapiirkondade faasiandmed ja soovitud polünoomi
        järgu ning tagastab koefitsendid piksli väärtuse arvutamiseks koos polünoomiga.
        """

        polynomials = []
        pvals = []

        for phase_row in self.phase_boxes:
            for phase_box in phase_row:
                # Esimesed kaks liiget massiivis kirjeldavad asukohta modulaatori
                # pinnal (ühikuteks pikslid), kus selline faasikoste saadi.
                loc_x = phase_box[0]
                loc_y = phase_box[1]
                phase_data = phase_box[2:]

                for i, phase_value in enumerate(phase_data):
                    polynomials.append(np.array(self.poly_lambda(loc_x, loc_y, phase_value)))
                    pvals.append(PVAL_RANGE[i])

        polynomials = np.array(polynomials)
        pvals = np.array(pvals)

        poly_transposed = np.matmul(np.transpose(polynomials), polynomials)
        pvals_transposed = np.matmul(np.transpose(polynomials), pvals)

        # Tagastame koefitsendid c_i
        return np.linalg.solve(poly_transposed, pvals_transposed)

    def calib_poly3d(self, phaseimg):
        """Funktsioon leitud koefitsientide kasutamiseks faasipildi halltoonide
        pildiks teisendamisel.

        Argumendid:
        phaseimg -- numpy.ndarray; Faasipilt, mille teisendame halltoonide pildiks.
        """

        x_grid, y_grid = np.ogrid[0:768, 0:1024]
        x_grid, y_grid = np.tile(x_grid, (1, 1024)), np.tile(y_grid, (768, 1))

        # Algne polünoom vabaliikmeta
        polyresult = self.poly_lambda(x_grid.astype(np.float64), y_grid.astype(np.float64)
                                      , phaseimg.astype(np.float64))[1:]

        # Lisame vabaliikme uuesti massiivina
        polyresult = np.insert(polyresult, 0, np.full((768, 1024), 1, dtype=np.float64), axis=0)

        # Nihutame telgi nii et igal pikslil on järgule vastavalt liikmeid 1d massiivis
        polyresult = np.einsum("kij->ijk", polyresult)

        # Skalaarkorrutis iga piksliga
        return np.clip(np.dot(polyresult, self.poly_coeffs), ZERO_GREYSCALE, 255).astype(np.uint8)

    def calib_boxing(self, phaseimg):
        """Funktsioon mõõdetud faasikostete otseseks kasutamiseks faasipildi
        halltoonide pildiks teisendamisel.

        Argumendid:
        phaseimg -- numpy.ndarray; Faasipilt, mille teisendame halltoonide pildiks.
        """

        x_steps = 768//self.n_box1d
        y_steps = 1024//self.n_box1d

        result = np.empty((768, 1024))

        for i in range(self.n_box1d):
            for j in range(self.n_box1d):

                phasecurve = self.phase_boxes[i, j, 2:]
                x_start, x_stop = i*x_steps, (i+1)*x_steps
                y_start, y_stop = j*y_steps, (j+1)*y_steps

                close_idx = self.closest_pval_idx(phasecurve, phaseimg[x_start:x_stop,
                                                                       y_start:y_stop])

                shape = (x_stop-x_start, y_stop-y_start)

                result[x_start:x_stop, y_start:y_stop] = PVAL_RANGE[close_idx].reshape(shape)

        return np.clip(result, ZERO_GREYSCALE, 255).astype(np.uint8)

    def calib_linear(self, phaseimg):
        """Funktsioon lineaarse kaliibrimise rakendamiseks, eeldades et faasimodulatsioonile
        2pi vastav halltoon on kõikjal ühesugune.

        Argumendid:
        phaseimg -- numpy.ndarray; Faasipilt, mille teisendame halltoonide pildiks.
        """

        result = (phaseimg/(2*np.pi)) * self.loc_2pi
        return np.clip(result, ZERO_GREYSCALE, 255).astype(np.uint8)

    def calib_glut(self, phaseimg):
        """Funktsioon globaalse kaliibrimiskõvera rakendamiseks. Arvestab koste
        mittelineaarsusega kuid ei arvesta koste ruumilise sõltuvusega.
        glut - Global Lookup Table

        Argumendid:
        phaseimg -- numpy.ndarray; Faasipilt, mille teisendame halltoonide pildiks.
        """

        fitfunc = np.poly1d(np.polyfit(self.global_phase_curve, PVAL_RANGE, 7))
        result = fitfunc(phaseimg)

        return np.clip(result, ZERO_GREYSCALE, 255).astype(np.uint8)

    def calib_interp(self, phaseimg):
        """Funktsioon faasipildi kaliibrimiseks interpoleeritud andmetega.
        Arvestab faasikoste ruumilise sõltuvusega.

        Argumendid:
        phaseimg -- numpy.ndarray; Faasipilt, mille teisendame halltoonide pildiks.
        """

        idximg = self.phase_to_idx(phaseimg)

        x_range = np.tile(np.arange(768), (1024, 1)).T
        y_range = np.tile(np.arange(1024), (768, 1))

        result = self.transform(idximg, x_range, y_range)

        return np.clip(result, ZERO_GREYSCALE, 255).astype(np.uint8)

    def gen_phasegrids(self, calibration_method):
        """Funktsioon n * 2D numpy massiivi tagastamiseks, mida kasutada teisel
        kaliibrimisel faasikoste määramise täpsuse hindamiseks.

        Argumendid:
        calibration_method -- function; faasimaski halltoonide pildiks teisendamisel
                              kasutatav funktsioon.
        """

        final = []

        print("\nKaliibrimine:", calibration_method.__name__)
        for i, phase_value in enumerate(PHASE_RANGE):
            progress((i+1)/PVAL_RANGE_SIZE)

            phase_mask = self.phase_grid_mask(phase_value)
            final.append(calibration_method(phase_mask))

        return np.array(final)
