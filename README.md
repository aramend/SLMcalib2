# SLMcalib2

Tegemist on Tartu Ülikooli füüsikalise optika laboris kasutamiseks mõeldud tarkvaraga, mis teeb mugavaks SLMi ruumilise faasikoste leidmise ning faasimaskidel kaliibrimiskõverate rakendamise. Kõikidel klassidel ja nende meetoditel on lisatud dokumentatsioon, mis peaks aitama tarkvara kasutamisega.

**Järgneb näide tarkvara kasutamisest mõõtmiste läbiviimiseks, faasikostete arvutamiseks ja kaliibrimismeetodite hindamiseks**.

## Mõõtmine

Mõõtmisteks on olemas skript, mille tööd juhitakse käsurea argumentidega.

```sh
$ ./run.py ../2018-07-03/
```

See käsk ütleb programmile tekitada difraktsioonivõred kõigi vaadeltavate halltoonidega ja salvestada pildid ülemkausta kausta 2018-07-03. Vahepeal pakub aga huvi ette arvutatud massiivide kuvamine SLMile ja nende pildistamine. Selleks on skriptil valikulised argumendid `-single` ja `-series`.

```sh
$ ./run.py . -single phase_mask.npy
```

`-single` ootab argumendiks ühe numpy massiivi nime, mida programm kuvab SLMile ja pildistab. Ülemises kasutusnäites on selle massiivi nimeks `phase_mask.npy`. Massiivi andmetüüp peaks olema `np.uint8`.

```sh
$ ./run.py ./series_images/ -series mask_series.npy
```

`-series` ootab samuti argumendina ühe numpy massiivi nime, kuid eeldatakse, et näites massiiv nimega `mask_series.npy` sisaldab mitu üksikut faasimaski, mida järjest SLMile kuvada ja pildistada.

## Mõõdetud andmete analüüs

Impordime analüüsiks vajaminevad moodulid ja sätestame neid.

```python
from src.slmcalib2 import DataAnalysis, Calibration
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline # Sobilik ainult Jupyteri keskkonnas, tavalises Pythoni skriptis eemaldada.
```

```python
font = {"weight" : "normal",
        "size"   : 20}

plt.rc("font", **font)
plt.rc("axes", linewidth=2)
```

Kõigepealt loeme sisse kaamera tehtud pildid numpy memmap viitega massiivi. Massiiv luuakse klassi DataAnalysis isendi loomisel, ning edasist andmetöötlust ja suhtlust kõvakettal oleva massiiviga lubab loodud klassi isend. Siin näites on pildid salvestatud ülemkausta kaustas `2018-07-03`. Memmap viide on vajalik, kuna pilte on palju, nad on kõik suure resolutsiooniga ja neil on 16-bitine sügavus. Seega on liiga kulukas neid analüüsi otstarbeks mälus hoida ja peab kasutama arvuti kõvaketast.

```python
analysis_instance = DataAnalysis("../2018-07-03/")
```

Selle jooksutamisel avaneb `cv2` aken, kus on vaja valida SLMi pildil seesmine servadega piiratud ala. Valmiseks on vaja määrata SLMil kuvatud suur ristkülik kastiga, mida saab tekitada hiirega vajutades ja õige suurusega kasti tirides. Kui must kast on joonistatud, siis valiku kinnitamiseks tuleb vajutada ESC. Seda ala määrates leiab programm ise üles kogu SLMi pinna kõikidel piltidel. Ala valikul kuvab programm ka määratud ristküliku kahe nurga koordinaadid, et oleks hiljem võimalik soovi korral sama ala valida. Sellisel juhul on mõtekas koordinaadid salvestada numpy sõnastikku ning kasutada `DataAnalysis` klassi võimalust temale manuaalselt koordinaadid ette anda.

```python
location = {"start_x" :  316,
            "start_y" :  161,
            "end_x"   : 1629,
            "end_y"   : 1901}

analysis_instance = sc2.DataAnalysis("../2018-07-03/", **location)
```

Kui andmed on sisse loetud, peaks tekkima piltide kausta nimele vastava nimega binaarne fail `datarr_+"nimi"` samasse kausta, kus paikneb analüüsitavate piltide kaust.

## Faasikosted ja kaliibrimisfunktsioonid

Kaliibrimist puudutavad funktsioonid ja muutujad leiab klassist `Calibration`. Selle isendi loomisel kasutatakse kahte argumenti, `analysis_instance` ja `n_box1d`. Esimesene on eelnevalt kasutatud andmete analüüsi klassi isend, mida `Calibration` kasutab enda töös. `n_box1d` on int tüüpi muutuja, millega määratakse vaadelda soovitavate alapiirkondade arv piki ühte SLMi telge. Teisel teljel valitakse sama palju alapiirkondi, nii et kokku uuritakse `n_box1d`^2 alapiirkonna faasikosteid.

```python
calibrator = sc2.Calibration(analysis_instance, 32) # Kokku 32 * 32 = 1024 alapiirkonna faasikosted
```

`Calibration` klassis kasutatakse ka klassi `DataAnalysis` isendifunktsioone, et leida faasikosted. Leitud faasikosteid võib vaadata `Calibration` klassi isendimuutuja `phase_boxes` alt. `Calibration` isendi loomisel arvutatakse kohe välja kõik vajalikud suurused võimalike kaliibrimismeetodite kasutuseks. See teeb isendi loomist pisut aeglaseks, kuid saavutatakse võit kiiruses hiljem faasimaskidele kaliibrimisfunktsioone rakendades.

Kuvame faasiväärtusi mingi fikseeritud halltooni korral, kasutades andmemassiivi `phase_boxes`. `phase_boxes` on $`n\times n\times (m+2)`$ suurusega numpy massiiv. $`n`$ on n_box1d, $`m`$ on tehtud piltide (andmepunktide) arv, mille teljel on iga alapiirkonna faasikoste. Iga alapiirkonna andmete algusesse on liidetud iga selle alapiirkonna $`x`$ ja $`y`$ koordinaadid.

Kuvame halltoonile 101 vastavaid faase, mis umbes vastab modulatsioonile $`\pi`$.

```python
plt.figure(figsize=(14, 10))
plt.imshow(calibrator.phase_boxes[:, :, 103], cmap="jet")
plt.colorbar()
plt.show()
```

![alt text](./img/103phase.png "Halltoonile 101 vastavad faasinihked")

On näha, et modulaatori faasikostes on suured ruumilised kõikumised, ühele halltoonile vastavad faasimodulatsioonid võivad üksteisest modulaatori erinevatel aladel üksteisest erineda $`0.8`$ radiaani. Selleks, et leida vajaminevad halltoonid kõikjal modulaatori pinnal faasimodulatsiooni $`\pi`$ tekitamiseks, peame kasutama faasikoste ruumilise struktuuriga arvestavaid meetodeid.

```python
# Loome sihtfaaside pildi
pi_plane = np.full((768, 1024), np.pi)

plt.figure(figsize=(14, 10))
plt.imshow(calibrator.calib_interp(pi_plane), cmap="jet")
plt.colorbar()
plt.show()
```

![alt text](./img/pi_interp.png "Kaliibritud")

Ülemisel pildil on kaliibrimiseks kasutatud interpoleerimise meetodit.

## Kaliibrimismeetodite võrdlus

Kaliibrimismeetodeid saab võrrelda teisel kaliibrimisel, kus rakendame difraktsioonivõre faasimaskidel erinevaid kaliibrimismeetodeid. Kui meetod töötab hästi, siis mõõdame sama faasi mida võre maski tekitamisel kasutati. `Calibration` klassil on isendifunktsioon `generate_phasecalib_array`, mis etteantud kaliibrimisfunktsiooniga tagastab võrede massiivi, mida SLMile kuvada ja pildistada. Erinevad võimalikud kaliibrimisfunktsioonid on `calib_linear`, `calib_poly3d`, `calib_boxing`, `calib_interp` ja `calib_glut`. Koodi sees on võimalik lähemalt uurida nende tööd.

Näitena loome teiseks kaliibrimiseks faasimaskide massiivi interpoleerimise funktsiooniga ja salvestame selle arvuti kõvakettale.

```python
func = calibrator.calib_interp
interp_series = calibrator.gen_phasegrids(func)

np.save("interp_series.npy", interp_series)
```

Salvestatud massiivi on võimalik üleval kirjeldatud mõõtmiskäskudega pildistada.

```sh
$ ./run.py ./second_interp/ -series interp_series.npy
```

Loome teise kaliibrimise andmetest uue `DataAnalysis` isendi ja kuvame uuesti mõõdetuid faase halltooni 101 juures.

```python
second_analysis = sc2.DataAnalysis("../second_interp/", **location)
new_boxes = second_analysis.get_phase_boxes(32)

plt.figure(figsize=(14, 10))
plt.imshow(new_boxes[:, :, 103], cmap="jet")
plt.colorbar()
plt.show()
```

![alt text](./img/interp_phases.png "Uus halltoonide tasand")

On näha, et ühele halltoonile vastavad palju sarnasemad faasinihked ning et eespool nähtud suured ruumilised erinevused on hästi kompenseeritud.