"""Gerchberg-Saxtoni algoritmi rakendavat funktsiooni sisaldav moodul"""

import numpy as np

def gerchberg_saxton(target, iterations=10, illumination=1):
    """Funktsioon, mis võtab argumendiks sihtpildi ja iteratsioonide arvu
    ning tagastab faasihologrammi, mille Fourier' pöördes leiab sihtpildi.

    Argumendid:
    target     -- np.ndarray; Sihtpilt, millele vastavat faasihologrammi otsitakse.
    iterations -- int; Algoritmi iteratsioonide arv.
    """

    # A, B on hologrammid, C ja D difraktsioonipildid
    # Alustame optimeerimist suvalise faasimaskiga
    A = np.fft.ifft2(target*np.exp(1j*np.random.random(target.shape)*2*np.pi))
    for _ in range(iterations):
        B = illumination*np.exp(1j*np.angle(A))
        C = np.fft.fft2(B)
        D = np.absolute(target) * np.exp(1j*np.angle(C))
        A = np.fft.ifft2(D)

    fin = np.fft.ifft2(np.fft.fftshift(np.fliplr(D)))

    return (np.angle(fin)+np.pi) % (2*np.pi)
