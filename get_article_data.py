#!/usr/bin/env python3

""" Skript faasikoste määramisel kasutavate parameetrite optimeerimiseks. """

import os

# Mõõdame
# Selle mõõtmise põhjal arvutada uued difraktsioonivõred erinevate kaliibrimismeetoditega

print("interp")
os.system(r".\run.py ./2018-07-11/article_interp/ -series article_interp.npy")
print("\npoly3d")
os.system(r".\run.py ./2018-07-11/article_poly3d/ -series article_poly3d.npy")
print("\nboxing")
os.system(r".\run.py ./2018-07-11/article_boxing/ -series article_boxing.npy")
print("\nlinear")
os.system(r".\run.py ./2018-07-11/article_linear/ -series article_linear.npy")
print("\nglut")
os.system(r".\run.py ./2018-07-11/article_glut/ -series article_glut.npy")

print("Valmis!")
