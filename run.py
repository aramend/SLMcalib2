#!/usr/bin/env python3

"""Python 3.5 skript mõõtmiste läbiviimiseks FLIR Grasshopper 3 kaameraga,
eesmärgiga määrata SLMi faasikoste.
"""

import os
import argparse
from src.slmcalib2 import DataGet
import numpy as np

PARSER = argparse.ArgumentParser(description="SLMi faasikoste leidmiseks\
                                 maskide kuvamine ja nende pildistamine.")

PARSER.add_argument("outputfolder", metavar="output", type=str,
                    help="Kausta nimi, kuhu tehtud pildid salvestatakse.")
PARSER.add_argument("-single", type=str, nargs=1, metavar="input",
                    help="1 pildi salvestamine etteantud maskiga.")
PARSER.add_argument("-series", type=str, nargs=1, metavar="input3darr",
                    help="n * 2D numpy massiiv, mille 2D alamassiive kuvame ja pildistame.")

ARGS = PARSER.parse_args()

if not os.path.exists(ARGS.outputfolder):
    os.makedirs(ARGS.outputfolder)

GETTER = DataGet(ARGS.outputfolder)

if ARGS.single != None:
    INPUT_ARR_NAME = ARGS.single[0]
    INPUT_ARR = np.load(INPUT_ARR_NAME)
    GETTER.single(INPUT_ARR, INPUT_ARR_NAME.strip(".npy")+".tiff")
elif ARGS.series != None:
    INPUT_ARR_NAME = ARGS.series[0]
    GETTER.series(INPUT_ARR_NAME)
else:
    GETTER.measure()

GETTER.kaamera.disconnect()
